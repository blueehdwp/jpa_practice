<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        #div {
            text-align: center;
        }

        #table {
            width: 30%;
            height: 100px;
            margin: auto;
            text-align: center;
            border: 1px solid black;
        }
    </style>
</head>
<body>
<div id="div">
    <h1>환자 목록</h1>
    <table id="table">
        <tr>
            <td>환자 이름</td>
            <td>나이</td>
            <td>도시</td>
            <td>병명</td>
        </tr>
    </table>
</div>
</body>
</html>