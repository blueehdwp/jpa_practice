<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        #div {
            text-align: center;
        }

        #table {
            width: 20%;
            height: 100px;
            margin: auto;
            text-align: center;
            border: 1px solid black;
        }
    </style>
</head>
<body>
<%
    String name = request.getParameter("pName");
    System.out.println(name);
%>
<div id="div">
    <h1>환자입력</h1>
</div>
<form>
    <table id="table">
        <tr>
            <td>환자이름</td>
            <td><Input type="text" name="pName"></td>
        </tr>
        <tr>
            <td>나이</td>
            <td><Input type="text" name="pAge"></td>
        </tr>
        <tr>
            <td>도시</td>
            <td><Input type="text" name="pCity"></td>
        </tr>
        <tr>
            <td>코드</td>
            <td><Input type="text" name="pVCode"></td>
        </tr>
        <tr>
            <td colspan="2">
                <button>입력</button>
            </td>
        </tr>
    </table>
</form>
</body>
</html>