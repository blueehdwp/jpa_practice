package com.example.chat.practice;

import java.sql.Connection;
import java.sql.DriverManager;

public class Connector {

    private final String URI = "jdbc:oracle:thin:@localhost:1521:XE";
    private final String username = "dongje";
    private final String password = "129323";

    public Connection getConnection() throws Exception {
        Class.forName("oracle.jdbc.OracleDriver");
        Connection conn = DriverManager.getConnection(URI, username, password);
        return conn;
    }

    public static void main(String[] args) {
        Connector connector = new Connector();

        System.out.println("연결테스트");
        System.out.println(connector);
    }
}

