package com.example.chat.handler;

import java.util.HashMap;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;


//채팅 핸들러
@Component
public class SocketHandler extends TextWebSocketHandler {
    private Logger log = LoggerFactory.getLogger(SocketHandler.class);
    HashMap<String, WebSocketSession> sessionMap = new HashMap<>();

    //세션 오픈
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        super.afterConnectionEstablished(session);
        sessionMap.put(session.getId(), session);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("type", "getId");
        jsonObject.put("sessionId", session.getId());
        session.sendMessage(new TextMessage(jsonObject.toJSONString()));

    }

    //세션 클로즈
    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus closeStatus) throws Exception {
        sessionMap.remove(session.getId());
        super.afterConnectionClosed(session, closeStatus);

    }

    //메시지 핸들러
    @Override
    public void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        String msg = message.getPayload();
        JSONObject object = JsonToObjectParser(msg);
        for (String key : sessionMap.keySet()) {
            WebSocketSession wss = sessionMap.get(key);
            try {
                wss.sendMessage(new TextMessage(object.toJSONString()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static JSONObject JsonToObjectParser(String jsonStr) {
        JSONParser parser = new JSONParser();
        JSONObject obj = null;
        try {
            obj = (JSONObject) parser.parse(jsonStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return obj;
    }
}
