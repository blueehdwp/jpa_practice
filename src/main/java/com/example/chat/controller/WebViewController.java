package com.example.chat.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class WebViewController {
    @GetMapping("/views/**")
    public String goURI(HttpServletRequest req) {
        return req.getRequestURI();
    }

    @GetMapping("/")
    public String goIndex() {
        return "/views/practice/practice";
    }
}
